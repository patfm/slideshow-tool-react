/**
 * Created by pmurray on 4/14/2017.
 */



var _initUI = function () {
    Reveal.initialize({
        width: "100%",
        height: "100%",
        margin: 0,
        minScale: 1,
        maxScale: 1,

        // Display controls in the bottom right corner
        controls: false,

        // Display a presentation progress bar
        progress: false,

        // Display the page number of the current slide
        slideNumber: false,

        // Push each slide change to the browser history
        history: true,

        // Enable keyboard shortcuts for navigation
        keyboard: true,

        // Enable the slide overview mode
        overview: false,

        // Vertical centering of slides
        center: false,

        // Enables touch navigation on devices with touch input
        touch: true,

        // Loop the presentation
        loop: true,

        // Change the presentation direction to be RTL
        rtl: false,

        // Randomizes the order of slides each time the presentation loads
        shuffle: false,

        // Turns fragments on and off globally
        fragments: true,

        // Flags if the presentation is running in an embedded mode,
        // i.e. contained within a limited portion of the screen
        embedded: false,

        // Flags if we should show a help overlay when the questionmark
        // key is pressed
        help: false,

        // Flags if speaker notes should be visible to all viewers
        showNotes: false,

        // Number of milliseconds between automatically proceeding to the
        // next slide, disabled when set to 0, this value can be overwritten
        // by using a data-autoslide attribute on your slides
        autoSlide: 0,

        // Stop auto-sliding after user input
        autoSlideStoppable: true,

        // Use this method for navigation when auto-sliding
        autoSlideMethod: Reveal.navigateNext,

        // Enable slide navigation via mouse wheel
        mouseWheel: true,

        // Hides the address bar on mobile devices
        hideAddressBar: true,

        // Opens links in an iframe preview overlay
        previewLinks: false,

        // Transition style
        transition: 'none', // none/fade/slide/convex/concave/zoom

        // Transition speed
        transitionSpeed: 'default', // default/fast/slow

        // Transition style for full page slide backgrounds
        backgroundTransition: 'fade', // none/fade/slide/convex/concave/zoom

        // Number of slides away from the current that are visible
        viewDistance: 3,

        // Parallax background image
        parallaxBackgroundImage: '', // e.g. "'https://s3.amazonaws.com/hakim-static/reveal-js/reveal-parallax-1.jpg'"

        // Parallax background size
        parallaxBackgroundSize: '', // CSS syntax, e.g. "2100px 900px"

        // Number of pixels to move the parallax background per slide
        // - Calculated automatically unless specified
        // - Set to 0 to disable movement along an axis
        parallaxBackgroundHorizontal: null,
        parallaxBackgroundVertical: null
    });

};


var _buildSlide = function(text, imageURL, tileFlag, mediaType){
    var $slideContainer = $("#slide_container")
    var slideNum = $("#slide_container section").length;

    if (tileFlag){
        _buildTile(slideNum, text ,imageURL, mediaType);
    } else {
        if (mediaType === "video"){
            var $slide = $("<section data-background-video='"+imageURL+"' data-background-size='contain'>");
        } else {
            var $slide = $("<section data-background-image='"+imageURL+"' data-background-size='contain'>");
        }

        var $slideTitle = $("<div>", {
            "class": "slide_title"});
        $slideTitle.text(text);
        var $home = $("<span>", {
            "class": "home",
            "click": function(){
                Reveal.slide( 0, 0 );
            }}).text("Home");
        $slideTitle.append($home);
        $slide.append($slideTitle);

        $slide.data("state", slideNum);
        $slide.appendTo($slideContainer);
    }
};

var _buildTile = function(slideNum, text, imageURL, mediaType){
    var $tileContainer = $("#grid");
    var $div = $("<div>", {
        "class": "content-tile",
        "click": function() {
            Reveal.slide( 0, slideNum );
        }
    });

    if (mediaType === "video"){
        $div.css("background-image", 'url("./imgs/video.png")');
    } else {
        $div.css("background-image", 'url("'+ imageURL +'")');
    }


    var $title = $("<span>", {
        "class": "title",
        "click": function() {
            Reveal.slide( 0, slideNum );
        }
    }).text(text);
    $("<div>").append($title).appendTo($div);
    $div.appendTo($tileContainer);
};


$(document).ready(function () {
    _initUI();
});