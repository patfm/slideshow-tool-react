/**
 * Created by pmurray on 5/22/2017.
 */


function revealBuilder(slides, dir, autoscroll){
    const fs = require('fs');
    const path = require('path');
    const fse = require('fs-extra');

    var indexHTML = `<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Title</title>
            <link rel="stylesheet" href="css/reveal/reveal.css">
            <link rel="stylesheet" href="css/style.css">
        </head>
        <body>
            <div class="reveal">
                <div class="slides">
                    <section id="slide_container">
                        <section id="contents" data-state="contents">
                            <div id="grid">

                            </div>
                        </section>
                    </section>
                </div>
            </div>
            <script src="js/jquery-3.2.1.min.js"></script>
            <script src="js/reveal.js"></script>
            <script src="js/frontend.js"></script>
            <script>
                <!-- replaceme -->
            </script>

        </body>
        </html>`;


    let buildSlideString = "";
    buildSlideString += "$(function(){";
    buildSlideString += `Reveal.configure({ autoSlide: ${autoscroll * 1000} }); \n`;
    buildSlideString += "});";
    if (!fs.accessSync(dir)) {
        try {
            fse.copySync(path.join(process.cwd(), "_scaffold"), path.join(dir));
            for (let a = 0; a < slides.length; a++) {
                buildSlideString += _buildAndCopy(slides[a]);
                for (let b = 0; b < slides[a].children.length; b++) {
                    buildSlideString += _buildAndCopy(slides[a].children[b]);
                }
            }
        } catch (err){
            console.error(err);
        }
        _buildIndexHTML(path.join(dir, "index.html"), buildSlideString);
    }


    function _buildAndCopy(slide){
        console.log("B&C", slide);
        let baseName = path.basename(slide.media).split("%20").join("");
        let nameStripped = path.basename(slide.media).split("%20").join("");
        let filePath = slide.media.split("%20").join(" ");
        _copy(filePath, `${dir}/imgs/${nameStripped}`);
        return `_buildSlide("${slide.title}", "./imgs/${baseName}" , ${slide.summaryFlag}, "${slide.mediaType}"); \n`;
    }

    function _copy(source, dest){
        fs.readFile(source, function (err, data) {
            if (err) return console.error(err);
            fs.writeFile(dest, data, function (err) {
                if (err) return console.error(err);
                console.log("Media copied: ", dest);
            });
        });
    }

    function _buildIndexHTML(dir, html){
        indexHTML = indexHTML.replace(/<!-- replaceme -->/g, buildSlideString);
        fs.writeFile(dir, indexHTML, 'utf8', function (err) {
            if (err) return console.error(err);
            console.log("index.html created: ", dir);
            alert("Slideshow successfully saved at: " + dir);
        });
    }
}

export default revealBuilder;
