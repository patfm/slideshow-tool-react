import React, { Component } from 'react';
import {observable, action, extendObservable, autorun, toJS} from 'mobx';
import { observer } from 'mobx-react';
import './App.css';
import revealBuilder from './revealBuilder'

//
// Component: Slide
//
class Slide {
    id = Math.random();
    @observable title;
    @observable media;
    @observable summaryFlag;
    @observable children;
    constructor(childOrObj){
        if(childOrObj instanceof Slide || childOrObj === undefined){
            this.title = "";
            this.media = "";
            this.summaryFlag = false;
            this.mediaType = "";
            this.children = [];
        } else {
            for (let [key, value] of Object.entries(childOrObj)) {
                if (key === "children"){
                    this.children = [];
                    value.forEach((child) => {
                        console.shallowCloneLog("child", child);
                        this.children.push(new Slide(child));
                    });
                } else {
                    this[key] = value;
                }
            }
        }
    }
    @action addChild(file){
        let childSlide = new Slide();
        if (file){
            childSlide.editMedia(file);
        }
        this.children.push(childSlide);
    }
    @action editTitle(title){
        this.title = title;
    }
    @action editMedia(url){
        if (!this.title){
            this.editTitle(url.split(/(\\|\/)/g).pop());
        }
        let fileExt = url ? url.split('.').pop() : "";
        switch (fileExt.toLowerCase()){
            case 'wmv':
            case 'webm':
            case 'mov':
            case 'mp4':
            case 'avi':
                this.mediaType = "video";
                break;
            default:
                this.mediaType = "image";
                break;
        }
        this.media = url;
    }
    @action toggleSummary(){
        this.summaryFlag = !this.summaryFlag;
    }
    @action removeChild(key) {
        this.children = this.children.filter(slide => slide.id !== key);
    }
    @action createChildren(files){
        const url = require("url");
        for (let i = 0; i < files.length; i++) {
            this.addChild(url.parse(files[i].path).href);
        }
    }
}

@observer
class ChildSlideView extends Component {
    render() {
        return <div>
            <FileUpload slide={this.props.slide}/>
            <input type="text" value={this.props.slide.title} onChange={ (e) => this.props.slide.editTitle(e.target.value)}/>
            <button type="button" className="removebtn" onClick={ (e) => this.props.parent.removeChild(this.props.slide.id)}>&#xff38;</button>
        </div>
    }
}

@observer
class SlideViewDefault extends Component {
    render() {
        let visible = this.props.slide.summaryFlag ? 'fa-eye' : 'fa-eye-slash';
        let cssClasses = `${visible} fa`;
        return <div>
            <button type="button" className="removebtn" onClick={ (e) => this.props.store.removeByKey(this.props.slide.id)}>&#xff38;</button>
            <input type="text" value={this.props.slide.title} onChange={ (e) => this.props.slide.editTitle(e.target.value)}/>
            <FileUpload slide={this.props.slide}/>
            <i className={cssClasses} onClick={ (e) => this.props.slide.toggleSummary()}/>
            <button type="button" className="addChildbtn" onClick={ (e) => this.props.slide.addChild()}>Add Child</button>
            <ul className="childSlideList" >
                {this.props.slide.children.map((slide, index) =>
                    <li>
                        <ChildSlideView store={this.props.store} parent={this.props.slide} slide={slide} key={slide.id} />
                    </li>
                )}
            </ul>
        </div>
    }
}

@observer
class FileUpload extends Component {
    constructor(){
        super();
    }
    openFileDialog(e) {
        this.fileDialog.click();
    }
    @action uploadMedia(e) {
        if (!e.target.value) return;
        const url = require("url");
        if (e.target.files.length > 1){
            this.props.slide.createChildren(e.target.files);
        } else {
            this.props.slide.editMedia(url.parse(e.target.files[0].path).href);
        }
    }
    render(){
        let fileExt = this.props.slide.media ? this.props.slide.media.split('.').pop() : "";
        let elStyle;
        let uploaded = this.props.slide.media ? "uploaded" : "";
        switch (fileExt){
            case 'wmv':
            case 'webm':
            case 'mp4':
                elStyle = {
                    backgroundImage: `url('./imgs/video.png')`,
                    backgroundSize: "contain",
                    backgroundPosition: "50% 50%"
                };
                break;
            default:
                elStyle = {
                    backgroundImage: `url('${this.props.slide.media}')`,
                    backgroundSize: "cover"
                };
                break;
        }
        return <div className="imageContainer">
            <label
                className={uploaded}
                onClick={(e) => this.openFileDialog(e)}
                style={elStyle}
            />
            <input
                className="fileDialog"
                type="file"
                multiple="multiple"
                onChange={(e) => this.uploadMedia(e)}
                ref={(input) => { this.fileDialog = input; }} />
        </div>
    }
}

//
// Component: SlideList
//
class SlideList {
    @observable slides = [];
    constructor(){
        this.autoscroll = 0;
    }
    @action removeByKey(key) {
        this.slides = this.slides.filter(slide => slide.id !== key);
    }
    @action addNew() {
        this.slides.push(new Slide());
    }
    @action switchPositions(oldPos, newPos) {
        this.slides[oldPos] = this.slides.splice(newPos, 1, this.slides[oldPos])[0];
    }
    @action changeAutoscroll(e){
        this.autoscroll = parseInt(e.target.value);
    }
    chooseDirectory(el){
        el.click();
    }
    exportData(e){
        let dir = e.target.value;
        revealBuilder(toJS(this.slides), dir, this.autoscroll);
    }
    saveData(e){
        const fs = require("fs");
        let dir = e.target.value;
        let data = JSON.stringify(toJS(this.slides));
        fs.writeFile(dir, data, 'utf8', function(){
            alert(`Slideshow saved at ${dir}`);
        });
    }
    @action loadData(e){
        const fs = require("fs");
        let dir = e.target.value;
        try {
            let data = fs.readFileSync(dir, 'utf8');
            let json = JSON.parse(data);
            this.slides = [];
            json.forEach((slide) => {
               this.slides.push(new Slide(slide));
            });
        }
        catch (e) {
            alert("Error loading save file.");
            console.log(e);
        }
    }
}

@observer
class SlideListView extends Component {
    constructor(){
        super();
        this.placeholder = document.createElement("li");
        this.placeholder.className = "placeholder";
    }
    dragStart = (e) => {
        // HTML5 Drag & Drop
        this.dragged = e.currentTarget;
        e.dataTransfer.effectAllowed = "move";
        e.dataTransfer.setData("text/html", e.currentTarget);
    }
    dragOver = (e) => {
        e.preventDefault();
        if(e.target.className === "placeholder" || e.target.tagName !== "LI") return;
        this.over = e.target;
        this.dragged.style.display = "none";
        if (this.over.dataset.position > this.dragged.dataset.position - 1){
            e.target.parentNode.insertBefore(this.placeholder, e.target.nextSibling);
        } else {
            e.target.parentNode.insertBefore(this.placeholder, e.target);
        }
    }
    @action dragEnd = (e) => {
        if(!this.over) return;
        this.props.store.switchPositions(this.dragged.dataset.position, this.over.dataset.position);
        this.dragged.style.display = "inline-block";
        this.dragged.parentNode.removeChild(this.placeholder);
    }
    render() {
        return <div>
            <ul className="slideList" >
                {this.props.store.slides.map((slide, index) =>
                    <li
                        key={slide.id}
                        data-position={index}
                        draggable="true"
                        onDragEnd={this.dragEnd}
                        onDragOver={this.dragOver}
                        onDragStart={this.dragStart}
                    >
                        <SlideViewDefault store={this.props.store} slide={slide} key={slide.id} />
                    </li>
                )}
            </ul>
        </div>
    }
}

@observer
class App extends Component {
    constructor(){
        super();
        this.store = new SlideList();
    }
    render() {
        return (
            <div className="App">
                <div className="nav">
                    <input is id="exportSlideShow" class="fileDialog" type="file" nwdirectory="nwdirectory" onChange={(e) => this.store.exportData(e)} />
                    <input is id="saveSlideShow" class="fileDialog" type="file" nwsaveas="filename.json" onChange={(e) => this.store.saveData(e)} />
                    <input is id="loadSlideShow" class="fileDialog" type="file" accept=".json" onChange={(e) => this.store.loadData(e)} />
                    <button onClick={(e) => this.store.addNew()}><i className="fa fa-plus"></i><div>Add Slide</div></button>
                    <button onClick={(e) => this.store.chooseDirectory(document.getElementById("exportSlideShow"))}><i className="fa fa-share-square-o"></i><div>Export Slideshow</div></button>
                    <button onClick={(e) => this.store.chooseDirectory(document.getElementById("saveSlideShow"))}><i className="fa fa-download"></i><div>Save Layout</div></button>
                    <button onClick={(e) => this.store.chooseDirectory(document.getElementById("loadSlideShow"))}><i className="fa fa-upload"></i><div>Load Layout</div></button>
                    <div className="autoscroll">
                        <span>autoscroll time (sec):</span>
                        <input type="number" onBlur={(e) => this.store.changeAutoscroll(e)} />
                    </div>
                </div>
                <h2>Slideshow Builder</h2>
                <SlideListView store={this.store} />
            </div>
        );
    }
}


console.shallowCloneLog = function(){
    var typeString = Function.prototype.call.bind(Object.prototype.toString)
    console.log.apply(console, Array.prototype.map.call(arguments, function(x){
        switch (typeString(x).slice(8, -1)) {
            case 'Number': case 'String': case 'Undefined': case 'Null': case 'Boolean': return x;
            case 'Array': return x.slice();
            default:
                var out = Object.create(Object.getPrototypeOf(x));
                out.constructor = x.constructor;
                for (var key in x) {
                    out[key] = x[key];
                }
                Object.defineProperty(out, 'constructor', {value: x.constructor});
                return out;
        }
    }));
}

export default App;

