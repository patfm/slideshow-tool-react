//
// Component: Slide
//
class Slide {
    id = Math.random();
    @observable title;
    @observable media;
    @observable summaryFlag;
    constructor(title){
        this.title = title || "";
        this.media = "";
        this.summaryFlag = true;
    }
    @action editTitle(title){
        this.title = title;
    }
    @action editImage(url){
        if (!this.title){
            this.editTitle(url.split(/(\\|\/)/g).pop());
        }
        this.media = url;
    }
    @action toggleSummary(){
        this.summaryFlag = !this.summaryFlag;
    }
}

@observer
class SlideView extends Component {
    render() {
        let visible = this.props.slide.summaryFlag ? 'fa-eye' : 'fa-eye-slash';
        let cssClasses = `${visible} fa`;
        return <div>
            <button type="button" className="removebtn" onClick={ (e) => this.props.store.removeByKey(this.props.slide.id)}>&#xff38;</button>
            <input type="text" value={this.props.slide.title} onChange={ (e) => this.props.slide.editTitle(e.target.value)}/>
            <FileUpload slide={this.props.slide}/>
            <i className={cssClasses} onClick={ (e) => this.props.slide.toggleSummary()}/>
        </div>
    }
}