import './slidelist.css';

var placeholder = document.createElement("li");
placeholder.className = "placeholder";

@observer
class SlideListView extends Component {
    @action dragStart = (e) => {
        // HTML5 Drag & Drop
        this.dragged = e.currentTarget;
        e.dataTransfer.effectAllowed = "move";
        e.dataTransfer.setData("text/html", e.currentTarget);
    }
    @action dragEnd = (e) => {
        this.props.store.switchPositions(this.dragged.dataset.position, this.over.dataset.position);
        this.dragged.style.display = "inline-block";
        this.dragged.parentNode.removeChild(placeholder);
    }
    @action dragOver = (e) => {
        e.preventDefault();
        if(e.target.className == "placeholder") return;
        this.over = e.target;
        this.dragged.style.display = "none";
        if (this.over.dataset.position > this.dragged.dataset.position - 1){
            e.target.parentNode.insertBefore(placeholder, e.target.nextSibling);
        } else {
            e.target.parentNode.insertBefore(placeholder, e.target);
        }
    }
    render() {
        return <div>
            <ul className="slideList" >
                {this.props.store.slides.map((slide, index) =>
                    <li
                        key={slide.id}
                        data-position={index}
                        draggable="true"
                        onDragEnd={this.dragEnd}
                        onDragOver={this.dragOver}
                        onDragStart={this.dragStart}
                    >
                        <SlideView store={this.props.store} slide={slide} key={slide.id} />
                    </li>
                )}
            </ul>
        </div>
    }
}

class SlideList {
    @observable slides = [];
    @action removeByKey(key) {
        this.slides = this.slides.filter(slide => slide.id !== key);
    }
    @action addNew() {
        this.slides.push(new Slide("TITLE"));
    }
    @action switchPositions(oldPos, newPos){
        this.slides[oldPos] = this.slides.splice(newPos, 1, this.slides[oldPos])[0];
    }
}
